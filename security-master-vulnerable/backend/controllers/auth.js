const { validationResult } = require('express-validator');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.signup = async (req, res, next) => {

  const errors = validationResult(req);
  console.log('details :')
  console.log(errors);
  if (!errors.isEmpty()) return;
 
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;

  try {
    const hashedPassword = await bcrypt.hash(password, 12);
   
    const userDetails = {
      name: name,
      email: email,
      password: password,
    };
    
    const result =  await User.save(userDetails);

    res.status(201).json({ message: 'User registered!' });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.login = async (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  try {
    const user = await User.find(email, password);
    console.log(user);
    if (!user) {
      const error = new Error('A user with this email could not be found.');
      error.statusCode = 401;
      throw error;
    }

    const storedUser = user[0][0];

    // const isEqual = await bcrypt.compare(password, storedUser.password);
    // const isEqual = password ==storedUser.password;
    // if (!isEqual) {
    //   const error = new Error('Wrong password!');
    //   error.statusCode = 401;
    //   throw error;
    // }

    const token = jwt.sign(
      {
        email: "email",
        userId: "id",
      },
      'secretfortoken',
      { expiresIn: '1h' }
    );
    res.status(200).json({ token: token /* ,userId: storedUser.id*/ });
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};
