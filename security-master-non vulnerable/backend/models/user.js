const db = require('../util/database');

module.exports = class User {
  constructor(name, email, password) {
    this.name = name;
    this.email = email;
    this.password = password;
  }

  static find(email) {
    // const query = `SELECT * FROM users WHERE name = '${email}'`;
    // console.log('find:')
    // db.connect((err) => {
    //   if (err) throw err;
    //   db.query("SELECT * FROM users WHERE name = " + email, (err, result) => {
    //     console.log(err);
    //     console.log('result :', result);
    //   });
    // })
     return db.execute('SELECT * FROM users WHERE email = ?', [email]);
  }

  static save(user) {
    console.log('user :', user);
    return db.execute(
      'INSERT INTO users (name, email, password) VALUES (?, ?, ?)',
      [user.name, user.email, user.password]
    );
  }
};
